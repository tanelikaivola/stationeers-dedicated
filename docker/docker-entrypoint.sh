#!/bin/bash

echo Running: /root/steamcmd.sh +login "${STEAM_USERNAME}" "${STEAM_PASSWORD}" +force_install_dir /server "+app_update ${STEAM_APPID} ${STEAM_BETA} ${STEAM_VALIDATE}" +quit
/root/steamcmd.sh +login "${STEAM_USERNAME}" "${STEAM_PASSWORD}" +force_install_dir /server "+app_update ${STEAM_APPID} ${STEAM_BETA} ${STEAM_VALIDATE}" +quit

echo Running: "$@"
exec "$@"

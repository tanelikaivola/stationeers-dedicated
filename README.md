# Stationeers Dedicated server on Docker

## Install

```
git clone https://gitlab.com/tanelikaivola/stationeers-dedicated.git
cp default.ini saves/
$EDITOR saves/default.ini # See section below
docker-compose up -d
```

Server also creates */server/default.ini* which you should just ignore, it's not used - only the */saves/default.ini* is.

### default.ini

- In the SERVER section, set your SERVERNAME, PASSWORD (optional) and DESCRIPTION
- In the RCON section, set your RCONPASSWORD to something unique
- Other settings are left as they are

### Required open ports

Ports that need to be available on the host and will be mapped to the container

- 27015/udp
- 27500/udp
- 27500/tcp (for browser rcon, optional)

If you wish to use different ports, you'll need to modify GAMEPORT and UPDATEPORT in *saves/default.ini* and ports for server in *docker-compose.yml* accordingly.

## Additional configuration options

### Running the beta

Modify *docker/Dockerfile* and remove the comment from beta-environment variable to run beta version.

### Selecting a custom map

Currently map is hardcoded in *docker/Dockerfile*.

```
CMD ["./rocketstation_DedicatedServer.x86_64", "-batchmode", "-nographics", "-autostart", "-autosaveinterval=300", "-basedirectory=/saves/", "-loadworld=Europa", "-worldtype=Europa"]
```

Change Europa to Mars / Moon / Space to switch to different map.

## Managing the server

RCON is only enabled in localhost by default.

- Connect to a running instance: `docker-compose exec server /bin/bash`
  - Run rcon (to store credentials): `srcon status` (and enter credentials)
  - Run rcon again (with stored creds): `srcon status`
- Or run it from the host: `docker-compose exec server srcon status`

### rcon using a browser

RCON can be used from external IP by adding `RCONIP=` and your external IP address you want to use to control the server.

You also need to uncomment the `27500/tcp` port from *docker-compose.yml* to make the port available to the Internet.

Note that this disables the ability to run rcon commands from inside of the container.

# Credits

- Thanks to matjam for https://github.com/matjam/stationeersrcon